function heatmap = dpm_heatmap(imfile, model_file)

im = color(imread(imfile));
load(model_file);
pyra = featpyramid(im, model); % Compute HOG feature for each scale
model_dp = gdetect_dp(pyra, model);

% Picking heatmap w/ max score seems to work based off of a few test cases
max_score = -inf;
max_level = 1;
for level = 1:pyra.num_levels
    score = model_dp.symbols(model.start).score{level};
    %size(score)
    %imshow(score);
    %keyboard;
    %HeatMap(score);
    m = max(max(score));
    if m > max_score
        max_score = m;
        max_level = level;
    end
end

heatmap = model_dp.symbols(model.start).score{max_level};

end